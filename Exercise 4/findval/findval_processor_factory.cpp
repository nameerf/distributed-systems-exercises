/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_FINDVAL

#include "legacyapps/findval/findval_processor_factory.h"
#include "legacyapps/findval/findval_processor.h"
#include "sys/processors/processor_keeper.h"
#include "sys/simulation/simulation_controller.h"
#include <iostream>

using namespace std;
using namespace shawn;

namespace findval
{

   // ----------------------------------------------------------------------
   void
   FindValProcessorFactory::
   register_factory( SimulationController& sc )
   throw()
   {
      sc.processor_keeper_w().add( new FindValProcessorFactory );
   }
   // ----------------------------------------------------------------------
   FindValProcessorFactory::
   FindValProcessorFactory()
   {
      //cout << "FindValProcessorFactory ctor" << &auto_reg_ << endl;
   }
   // ----------------------------------------------------------------------
   FindValProcessorFactory::
   ~FindValProcessorFactory()
   {
      //cout << "FindValProcessorFactory dtor" << endl;
   }
   // ----------------------------------------------------------------------
   std::string
   FindValProcessorFactory::
   name( void )
   const throw()
   {
      return "findval";
   }
   // ----------------------------------------------------------------------
   std::string
   FindValProcessorFactory::
   description( void )
   const throw()
   {
      return "Shawn FindVal Processor";
   }
   // ----------------------------------------------------------------------
   shawn::Processor*
   FindValProcessorFactory::
   create( void )
   throw()
   {
      return new FindValProcessor;
   }
}

#endif
