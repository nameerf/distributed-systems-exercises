/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_FINDVAL

#include "legacyapps/findval/findval_processor.h"
#include "legacyapps/findval/findval_message.h"
#include "sys/simulation/simulation_controller.h"
#include "sys/node.h"
#include "sys/misc/tokenize.h"
#include <iostream>
#include <limits>
#include <cmath>

namespace findval
{

   // Constructor
   FindValProcessor::
   FindValProcessor()
		:	connected_  (false),	
			gateway_	(false),
			hop_dist_   (std::numeric_limits<int>::max()),
			parent_     (""),
			min_		(0),
			max_		(0),
			avg_		(0),
			sum_		(0)
   {}
   // ----------------------------------------------------------------------
   FindValProcessor::
   ~FindValProcessor()
   {}

   void
   FindValProcessor::
   special_boot( void ) throw()
   {
       // Edo epilegoume thn diergasia pou 8a einai to gateway tou diktuou mas
       // kai 8a mazeyei tis times apo tis ypoloipes diergasies tou diktyou.
       // Epilegoume os gateway thn diergasia me id = 0
	   if(owner_w().id() == 0) {
		   gateway_ = true;
		   connected_ = true;
		   hop_dist_ = 0;
           known_nodes_ = std::vector<std::string>();   
           
           // To gateway stelnei to proto flooding mhnyma
           send(new FindValFloodingMessage(1, owner_w().label()));
           //std::cout << owner_w().label() << ": Gateway sending first flood message " << std::endl;           
	   }
   }

   // ----------------------------------------------------------------------
   void
   FindValProcessor::
   boot( void ) throw()
   {
	   // Tis metablhtes ton diergasion tis arxikopoioyme ston constructor
	   
	   // Pairnoume thn timh ths metablhths diameter pou exoume orisei sto configuration file findval.conf
       const shawn::SimulationEnvironment& se = owner().world().simulation_controller().environment();
       diameter_ = se.optional_int_param("diameter", 0);

       // Diabazoume apo thn ka8e diergasia thn etiketa (tag) me onoma "input_value"
       // ths opoias h timh 8a stalei sto gateway
       shawn::TagHandle tag = owner_w().find_tag_w("input_value");
       if(tag.is_not_null()) {
		   input_tag_ = dynamic_cast<shawn::IntegerTag *>(tag.get());
	   }
	   // Apo8hkeuoume sthn input_value_ thn timh pou diabasame apo to arxeio ths topologias
	   input_value_ = input_tag_->value();
	   // Arxika h diergasia 0 den exei parei kamia timh apo tis upoloipes diergasies
	   // kai ara 8etei thn timh ths os thn trexousa elaxisth, trexousa megisth kai trexon a8roisma.
	   if(gateway_) {
		   min_ = input_value_;
		   max_ = input_value_;
		   sum_ = input_value_;
	   }
   }
   // ----------------------------------------------------------------------

   bool
   FindValProcessor::
   process_message( const shawn::ConstMessageHandle& mh ) 
      throw()
   {
      const FindValFloodingMessage* floodmsg = dynamic_cast<const FindValFloodingMessage*> ( mh.get() );
      
      if ( floodmsg != NULL && owner_w() != floodmsg->source())
      {
         handle_flooding_message_node( *floodmsg );
         return true;
      }

      const FindValDataMessage* datamsg = dynamic_cast<const FindValDataMessage*> ( mh.get() );

      if ( datamsg != NULL && owner_w().label() == datamsg->destination())
      {
          if(gateway_)
              handle_data_message_gateway( *datamsg );
          else
              handle_data_message_node( *datamsg );

         return true;
      }
      
      return shawn::Processor::process_message( mh );
   }
   // ----------------------------------------------------------------------

   void
   FindValProcessor::
   work( void ) throw()
   {
	   // An exoun perasei 2*diametro to plh8os guroi, shmainei pos h diergasia 0 8a exei parei kai
	   // tis times apo ta fulla tou dentrou kai ara o algori8mos/eksomoiosh termatizei.
	   if(simulation_round() == 2*diameter_) {
		   if(gateway_) {
			   // H diergasia 0 ektyponei ta telika apotelesmata
				std::cout << "Total number of distinct values received by gateway: " << known_nodes_.size() << std::endl; 
				std::cout << "The minimum value of network is " << min_ << std::endl;
				std::cout << "The maximum value of network is " << max_ << std::endl;
				std::cout << "Average value of network is " << avg_ << std::endl;
				std::cout << "Sum of all values of network is " << sum_ << std::endl;
		   }
		   // Apenergopoioume thn ka8e diergasia
		   set_state(shawn::Processor::Inactive);
	   }
       else if(gateway_) {
		   // Apostolh flooding mhnymatos apo thn diergasia 0
		   send(new FindValFloodingMessage(1, owner_w().label()));
		   // An h diergasia 0 exei paralabei toulaxiston 1 mhnyma ektyponei tis trexouses times
		   if(known_nodes_.size() > 0) {
		   // H diergasia 0 (gateway) ekyponei se ka8e guro poses times exei paralabei os tora, ka8os kai thn 
		   // trexousa elaxisth timh, trexousa megisth timh, trexousa mesh timh kai to trexon a8roisma
				std::cout << "Total number of distinct values received by gateway so far: " << known_nodes_.size() << std::endl; 
				std::cout << "Current minimum value: " << min_ << std::endl;
				std::cout << "Current maximum value: " << max_ << std::endl;
				std::cout << "Current average value: " << avg_ << std::endl;
				std::cout << "Sum of values so far: " << sum_ << std::endl;
			}
       }
       else {
		   // Oles oi upoloipes diergasies stelnoun ena data mhnyma pros tous goneis tous me tis times tous 
		   send(new FindValDataMessage(owner_w().label(), parent_, input_value_));
	   }
   }
   // ----------------------------------------------------------------------

   void
   FindValProcessor::
   handle_flooding_message_node( const FindValFloodingMessage& floodmsg ) throw()
   {
      // An to hop count tou message einai megalytero h iso to aporiptoume
      if(hop_dist_ <= floodmsg.hops())
         return;

      //INFO( logger(), owner_w().label() << ": Joint tree under parent " << floodmsg.parent()
      //               << " hops to gateway: " << floodmsg.hops());

      // Pairnoume apo to message tis plirofories gia to hop count kai ton parent node
      // kai tis apo8hkeyoume
	  hop_dist_ = floodmsg.hops();
      parent_ = floodmsg.parent();

      // Tora h diergasia/kombos einai syndedemenh
      connected_ = true;

      // H diergasia proo8ei to flooding mhnyma
      send(new FindValFloodingMessage(hop_dist_+1, owner_w().label()));
   }
   // ----------------------------------------------------------------------

   void
   FindValProcessor::
   handle_data_message_node( const FindValDataMessage& datamsg ) throw()
   {	   
      // Mono oi sundedemenes diergasies/komboi mporoun na proo8hsoun data mhnymata
      if(connected_)
      {
          send(new FindValDataMessage(datamsg.sender(), parent_, datamsg.value()));
      }
   }
   // ----------------------------------------------------------------------

   void
   FindValProcessor::
   handle_data_message_gateway( const FindValDataMessage& datamsg ) throw()
   {                     
       // Elegxoume oles tis apo8hkeumenes diergasies pou mas exoun steilei thn timh tous. 
       // An h diergasia pou molis esteile mhnyma einai stis "gnostes" (apo8hkeumenes)
       // den thn apo8hkeuoume kai epistrefoume
       for(int i=0; i<known_nodes_.size(); i++) {
		   if(known_nodes_[i] == datamsg.sender()) {
			   return;
		   }
		}
       
       // An h diergasia pou esteile mhnyma den einai apo8hkeumenh stis "gnostes"
       // diergasies thn apo8hkeuoume
       known_nodes_.push_back(datamsg.sender());
       // An h timh pou molis phrame einai mikroterh apo thn trexousa elaxisth
	   // thn apo8hkeuoume os thn trexousa elaxisth
       if(datamsg.value() < min_) {
		   min_ = datamsg.value();
	   }
	   // An h timh pou molis phrame einai megaluterh apo thn trexousa megisth
	   // thn apo8hkeuoume os thn trexousa megisth			
       if(datamsg.value() > max_) {
		   max_ = datamsg.value();
	   }
	   // Pros8etoume thn timh pou phrame sto trexon a8roisma
       sum_ += datamsg.value();
       // Ypologizoume ton trexonnta meso oro, diairontas to trexon a8roisma
       // me ton ari8mo ton gnoston diergasion +1 gia thn diergasia 0
       avg_ = (float)sum_/((float)known_nodes_.size()+1);
   }
   // -----------------------------------------------------------------------
}
#endif
