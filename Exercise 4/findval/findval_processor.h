/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#ifndef __SHAWN_LEGACYAPPS_FINDVAL_PROCESSOR_H
#define __SHAWN_LEGACYAPPS_FINDVAL_PROCESSOR_H
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_FINDVAL

#include "sys/taggings/basic_tags.h"
#include "sys/processor.h"
#include "sys/event_scheduler.h"
#include "legacyapps/findval/findval_message.h"
#include <string.h>
#include <vector>

namespace findval
{
   /**
    */
   class FindValProcessor
       : public shawn::Processor
   {
   public:
      ///@name Constructor/Destructor
      ///@{
      FindValProcessor();
      virtual ~FindValProcessor();
      ///@}

      ///@name Inherited from Processor
      ///@{
      /**
       */
      virtual void boot( void ) throw();
      /**
       */
      virtual bool process_message( const shawn::ConstMessageHandle& ) throw();
      /**
       */
      virtual void work( void ) throw();
      ///@}

      virtual void special_boot( void ) throw();


   private:
       
      ///@name Message Handling
      ///@{
      /**
       */
      void handle_flooding_message_node( const FindValFloodingMessage& flooding ) throw();
      /**
       */
      void handle_data_message_node( const FindValDataMessage& state ) throw();
      /**
       */
      void handle_data_message_gateway( const FindValDataMessage& state ) throw();
      ///@}

      /// H diametros toy diktuou (thn pairnoume apo to .conf arxeio)
      int diameter_;

	  /// Thn 8etoume se true gia thn diergasia 0 (id=0)
      bool gateway_;
      
      /// Ka8orizei an mia diergasia/kombos einai syndedemenh sto dentro
      /// (kai ara kserei poios einai o pateras ths)
      bool connected_;

      /// H trexousa apostash apo th riza tou dentrou
      int hop_dist_;

      /// O pateras ths ka8e diergasias (an yparxei kai den einai null)
      std::string parent_;      
      
      /// To input tag poy xrhsimopoioume gia na diabasoume ta tags ton diergasion/kombon 
      /// apo tis ekastote topologies
      shawn::IntegerTag *input_tag_;
      
      /// H timh eisodou ths ka8e diergasias pou diabazoume apo thn ekastote topologia
      int input_value_;
      
      /// Enas vector pou apo8hkeyei tis diakekrimenes diergasies/kombous pou
      /// stelnoun mhnumata piso sthn diergasia 0 me tis times tous
      std::vector<std::string> known_nodes_;
      
      /// H elaxisth timh tou diktuou
      int min_;
      
      /// H megisth timh tou diktuou
      int max_;
      
      /// To a8roisma ton timon olon ton diergasion
      int sum_;
      
      /// O mesos oros ton timon olon ton diergasion
      float avg_;
      
   };

}

#endif
#endif
