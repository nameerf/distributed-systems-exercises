/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_FINDVAL

#include "legacyapps/findval/findval_message.h"

namespace findval
{
    
	// ----------------------------------------------------------------------
   FindValFloodingMessage::
   FindValFloodingMessage(int hops, std::string source)
    : hops_   ( hops ),
      source_ ( source )
	{}
	// ----------------------------------------------------------------------
   FindValFloodingMessage::
	~FindValFloodingMessage()
	{}

	// ----------------------------------------------------------------------
   FindValDataMessage::
   FindValDataMessage(std::string sender, std::string destination, int value)
    : sender_ ( sender ),
      destination_ ( destination ),
      value_   ( value )
	{}
	// ----------------------------------------------------------------------
   FindValDataMessage::
	~FindValDataMessage()
	{}

}
#endif
