/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#ifndef __SHAWN_LEGACYAPPS_FINDVAL_MESSAGE_H
#define __SHAWN_LEGACYAPPS_FINDVAL_MESSAGE_H
#include "_legacyapps_enable_cmake.h"

#ifdef ENABLE_FINDVAL

#include "sys/message.h"
#include <string.h>

namespace findval
{

   class FindValFloodingMessage
       : public shawn::Message
   {
   public:
      FindValFloodingMessage(int, std::string);
      virtual ~FindValFloodingMessage();

      inline int hops( void ) const throw()
      {
          return hops_;
      };

      inline std::string parent( void ) const throw()
      {
          return source_;
      };    
      
   private:
      int hops_;
      std::string source_;
      
   };

   class FindValDataMessage
       : public shawn::Message
   {
   public:
      FindValDataMessage(std::string, std::string, int);
      virtual ~FindValDataMessage();

      inline std::string sender( void ) const throw()
      {
          return sender_;
      };

      inline std::string destination( void ) const throw()
      {
          return destination_;
      };

      inline int value( void ) const throw()
      {
          return value_;
      };

   private:
      int value_;
      std::string sender_;
      std::string destination_;

   };
       
}

#endif
#endif
