/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_MUTEX

#include "legacyapps/mutex/mutex_processor.h"
#include "legacyapps/mutex/mutex_message.h"
#include "sys/simulation/simulation_controller.h"
#include <iostream>
#include <limits>
#include <cmath>

#define ROUNDS_IN_CS 3

namespace mutex
{

   MutExProcessor::
   MutExProcessor()
		: coordinator_		( false ),
		  round_in_			( -1 ),
		  _CS_free_			( true )
   {}
   // ----------------------------------------------------------------------
   
   MutExProcessor::
   ~MutExProcessor()
   {}
   // ----------------------------------------------------------------------
   
   void
   MutExProcessor::
   special_boot( void )
      throw()
   {}
   // ----------------------------------------------------------------------

   void
   MutExProcessor::
   boot( void )
      throw()
   {
		//initialize the algorithm variables
		
		//epilegoume thn diergasia 0 na einai o syntonisths
		if(id() == 0)
			coordinator_ = true;
		
		// Diabazoume apo thn ka8e diergasia thn etiketa (tag) me onoma "input_value"
		shawn::TagHandle tag = owner_w().find_tag_w("input_value");
		if(tag.is_not_null()) {
			input_tag_ = dynamic_cast<shawn::IntegerTag *>(tag.get());
		}      
		// Apo8hkeuoume sthn input_value_ thn timh pou diabasame apo to arxeio ths topologias
		input_value_ = input_tag_->value();
		
		INFO(logger() , id() << ": my input value is :" << input_value_ << std::endl);       
   }
   // ----------------------------------------------------------------------

   bool
   MutExProcessor::
   process_message( const shawn::ConstMessageHandle& mh ) 
      throw()
   {

      const MutExRequestMessage* RequestMessage = dynamic_cast<const MutExRequestMessage*> ( mh.get() );

      if ( RequestMessage != NULL && owner() != RequestMessage->source())
      {
         handle_request_message_node( *RequestMessage  );
         return true;
      }

      const MutExReplyMessage* ReplyMessage = dynamic_cast<const MutExReplyMessage*> ( mh.get() );
      
      if ( ReplyMessage != NULL && owner() != ReplyMessage->source())
      {
         handle_reply_message_node( *ReplyMessage );
         return true;
      }

      const MutExReleaseMessage* ReleaseMessage = dynamic_cast<const MutExReleaseMessage*> ( mh.get() );

      if ( ReleaseMessage != NULL && owner() != ReleaseMessage->source())
      {
         handle_release_message_node( *ReleaseMessage );
         return true;
      }

      return shawn::Processor::process_message( mh );
   }
   // ----------------------------------------------------------------------

   void
   MutExProcessor::
   work( void )
      throw()
   {
	
		// An h diergasia einai o syntonisths kai an h oura pou diathrei den einai kenh 
		// kai to KT einai eley8ero, o suntonisths stelnei mhnyma reply sthn proth diergasia
		// ths ouras gia na mpei sto KT kai th bgazei apo thn oura.
		if(coordinator_) {
			if(!req_processes_.empty() && _CS_free_) {
				std::cout << "Coordinator: access given to the process " << req_processes_.front() 
					      << " to get into the CS" << std::endl;
				send(new MutExReplyMessage(req_processes_.front()));
				req_processes_.pop();
				_CS_free_ = false;
			}		   
		}
		// Allios an h diergasia den einai o syntonisths elegxoume an to id ths diergasias 
		// diaireitai akribos me ta dyo teleytaia pshfia tou AM mas (15) kai an diaireitai
		// KAI o trexon guros ths eksomoioshs einai isos me thn timh ths diergasias 
		// stelnei ena neo request mhnyma ston syntonisth gia na mpei sto KT.
		else if((id() % 15 == 0) && (simulation_round() == input_value_)) {
			//std::cout << "The process " << id() << " wants to get to the CS" << std::endl;
			send(new MutExRequestMessage(id()));
		}
		// An h diergasia teleiose me to KT stelnei ena release mhnyma ston syntonisth
		// oti bghke apo to KT.
		if(round_in_ != -1 && round_in_+ROUNDS_IN_CS == simulation_round()) {
			std::cout << "Process " << id() << ": getting out of the CS..." << std::endl;		   
			send(new MutExReleaseMessage());
		}	   
	   
   }
   // ----------------------------------------------------------------------

   void
   MutExProcessor::
   handle_request_message_node( const MutExRequestMessage& requestMsg )
      throw()
   {
       //Handle the new request mutex message
       
       // An h diergasia einai o syntonisths topo8etei sthn FIFO oura ths thn aithsh pou hr8e
       if(coordinator_) {
		   std::cout << "Coordinator: " << "the process " << requestMsg.originator() 
					 << " requested access to the CS" << std::endl;
		   req_processes_.push(requestMsg.originator());
	   }
	   
   }
   // ----------------------------------------------------------------------

   void
   MutExProcessor::
   handle_reply_message_node( const MutExReplyMessage& replyMsg )
      throw()
   {
       //Handle the new reply mutex message
       
       // An to id() pou periexei to reply mhnyma pou esteile o syntonisths einai idio
       // me to id() ths diergasias pou ekane request na mpei sto KT, h diergasia mpainei sto KT.
       if(replyMsg.destination_id() == id()) {
		   std::cout << "Process " << id() << ": getting into the CS..." << std::endl;
		   // apo8hkeuoume ton gyro pou h diergasia mpainei sto KT
		   round_in_ = simulation_round();
	   }

   }
   // ----------------------------------------------------------------------

   void
   MutExProcessor::
   handle_release_message_node( const MutExReleaseMessage& releaseMsg )
      throw()
   {
       //Handle the new release mutex message
       
       // An h diergasia einai o syntonisths kai dex8hke release mhnyma apo kapoia
       // diergasia, 8etei thn metablhth _CS_free_ se true pragma pou shmainei pos
       // to KT einai pali eley8ero.
       if(coordinator_) {
		   std::cout << "Coordinator: " << "the CS is now free" << std::endl;
		   _CS_free_ = true;
	   }
	   
   }
   // ----------------------------------------------------------------------

}
#endif
