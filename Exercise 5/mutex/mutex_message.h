/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#ifndef __SHAWN_LEGACYAPPS_MUTEX_MESSAGE_H
#define __SHAWN_LEGACYAPPS_MUTEX_MESSAGE_H
#include "_legacyapps_enable_cmake.h"

#ifdef ENABLE_MUTEX

#include "sys/message.h"
#include <string.h>

namespace mutex
{

   class MutExReplyMessage
       : public shawn::Message
   {
   public:
      MutExReplyMessage(int destination_id);
      virtual ~MutExReplyMessage();

      inline int destination_id( void ) const throw()
      {
          return destination_id_;
      };

   private:
      int destination_id_;
   };

   class MutExReleaseMessage
       : public shawn::Message
   {
   public:
      MutExReleaseMessage();
      virtual ~MutExReleaseMessage();

   };

   class MutExRequestMessage
       : public shawn::Message
   {
   public:
      MutExRequestMessage(int source_id);
      virtual ~MutExRequestMessage();

      inline int originator( void ) const throw()
      {
          return source_id_;
      };

   private:
      int source_id_;
   };
   
}
#endif
#endif
