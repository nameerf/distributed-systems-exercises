/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_MUTEX

#include "legacyapps/mutex/mutex_processor_factory.h"
#include "legacyapps/mutex/mutex_processor.h"
#include "sys/processors/processor_keeper.h"
#include "sys/simulation/simulation_controller.h"
#include <iostream>

using namespace std;
using namespace shawn;

namespace mutex
{

   // ----------------------------------------------------------------------
   void
   MutExProcessorFactory::
   register_factory( SimulationController& sc )
   throw()
   {
      sc.processor_keeper_w().add( new MutExProcessorFactory );
   }
   // ----------------------------------------------------------------------
   MutExProcessorFactory::
   MutExProcessorFactory()
   {
      //cout << "HelloworldProcessorFactory ctor" << &auto_reg_ << endl;
   }
   // ----------------------------------------------------------------------
   MutExProcessorFactory::
   ~MutExProcessorFactory()
   {
      //cout << "HelloworldProcessorFactory dtor" << endl;
   }
   // ----------------------------------------------------------------------
   std::string
   MutExProcessorFactory::
   name( void )
   const throw()
   {
      return "mutex";
   }
   // ----------------------------------------------------------------------
   std::string
   MutExProcessorFactory::
   description( void )
   const throw()
   {
      return "Shawn mutex Processor";
   }
   // ----------------------------------------------------------------------
   shawn::Processor*
   MutExProcessorFactory::
   create( void )
   throw()
   {
      return new MutExProcessor;
   }
}

#endif
