/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#ifndef __SHAWN_LEGACYAPPS_MUTEX_PROCESSOR_H
#define __SHAWN_LEGACYAPPS_MUTEX_PROCESSOR_H
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_MUTEX

#include "sys/processor.h"
#include "sys/event_scheduler.h"
#include "sys/node.h"
#include "legacyapps/mutex/mutex_message.h"
#include "sys/taggings/basic_tags.h"
#include <string.h>
#include <queue>

namespace mutex
{

   /**
    */
   class MutExProcessor
       : public shawn::Processor
   {
   public:
      ///@name Constructor/Destructor
      ///@{
      MutExProcessor();
      virtual ~MutExProcessor();
      ///@}

      ///@name Inherited from Processor
      ///@{
      /**
       */
      virtual void boot( void ) throw();
      /**
       */
      virtual bool process_message( const shawn::ConstMessageHandle& ) throw();
      /**
       */
      virtual void work( void ) throw();
      ///@}

      virtual void special_boot( void ) throw();

   private:
      ///@name Message Handling
      ///@{
      /**
       */
      void handle_request_message_node( const MutExRequestMessage&  requestMsg) throw();
      
      void handle_reply_message_node( const MutExReplyMessage&  replyMsg) throw();

      void handle_release_message_node( const MutExReleaseMessage&  releaseMsg) throw();
      
       //// Metablhth pou ka8orizei an h diergasia einai o suntonisths
      bool coordinator_;

       //// The initial value of this node that read its as input from a tag
      int input_value_;

       ////  The tag that contains the input value for this node (is loaded from the .xml file)
      shawn::IntegerTag *input_tag_; 

       //// The total number of nodes for this simulation
      int node_count_;       
      
       //// H oura pou diathrei h diergasia syntonisths gia tis eiserxomenes aithseis
      std::queue<int> req_processes_;
      
       //// O gyros ths eksomoioshs ton opoio h diergasia mpainei sto KT
      int round_in_;
      
       //// Metablhth pou ka8orizei an to KT einai eley8ero h oxi
      bool _CS_free_;
       
   };

}

#endif
#endif
