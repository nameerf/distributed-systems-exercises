/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_MUTEX

#include "legacyapps/mutex/mutex_message.h"

namespace mutex
{

	// ----------------------------------------------------------------------
   MutExRequestMessage::
   MutExRequestMessage(int source_id)
    : source_id_   ( source_id )
    {}
	// ----------------------------------------------------------------------
   MutExRequestMessage::
	~MutExRequestMessage()
	{}

	// ----------------------------------------------------------------------
   MutExReplyMessage::
   MutExReplyMessage(int destination_id)
    : destination_id_   ( destination_id )
    {}
	// ----------------------------------------------------------------------
   MutExReplyMessage::
	~MutExReplyMessage()
	{}

	// ----------------------------------------------------------------------
   MutExReleaseMessage::
   MutExReleaseMessage()
    {}
	// ----------------------------------------------------------------------
   MutExReleaseMessage::
	~MutExReleaseMessage()
	{}

}
#endif
