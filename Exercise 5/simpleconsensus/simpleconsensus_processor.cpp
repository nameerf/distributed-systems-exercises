/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_SIMPLECONSENSUS

#include "legacyapps/simpleconsensus/simpleconsensus_processor.h"
#include "legacyapps/simpleconsensus/simpleconsensus_message.h"
#include "sys/simulation/simulation_controller.h"
#include "sys/node.h"
#include "sys/taggings/basic_tags.h"
#include <iostream>
#include <limits>
#include <cmath>
#include <algorithm>

namespace simpleconsensus {

    SimpleConsensusProcessor::
    SimpleConsensusProcessor()
      : UNKNOWN_VALUE    ( std::numeric_limits<int>::max() ),
		max_freq 		 ( 0 ),
        maj_value		 ( UNKNOWN_VALUE )
    {
    }
    // ----------------------------------------------------------------------

    SimpleConsensusProcessor::
    ~SimpleConsensusProcessor() {
    }
    // ----------------------------------------------------------------------

    void
    SimpleConsensusProcessor::
    special_boot(void)
    throw () {
    }
    // ----------------------------------------------------------------------

    void
    SimpleConsensusProcessor::
    boot(void)
    throw () {
		
		//initialize the algorithm variables
		
		const shawn::SimulationEnvironment& se = owner().world().simulation_controller().environment();
		
		diameter_ = se.optional_int_param("diameter",0);
		node_count_  = owner_w().world_w().node_count();
		value_list_ = new int[node_count_];
		
		// Enter the code for the Tags example here
		// Diabazoume apo thn ka8e diergasia thn etiketa (tag) me onoma "input_value"
		shawn::TagHandle tag = owner_w().find_tag_w("input_value");
		if(tag.is_not_null()) {
		  input_tag_ = dynamic_cast<shawn::IntegerTag *>(tag.get());
		}      
		// Apo8hkeuoume sthn input_value_ thn timh pou diabasame apo to arxeio ths topologias
		input_value_ = input_tag_->value();
		
		// Pairnoume to upoloipo ths diaireshs ths timhs ths diergasias me ta dyo teleytaia pshfia tou AM mas
		input_value_ %= 15;
		
		// Init the array with the node values
		// In the beginning we do not know any value
		for(int i=0;i<node_count_;i++)
		{
		  value_list_[i] = UNKNOWN_VALUE;
		}
		
		value_list_[id()] = input_value_;
		INFO(logger() , id() << ": my input value is :" << input_value_ << std::endl);

    }
    // ----------------------------------------------------------------------

    bool
    SimpleConsensusProcessor::
    process_message(const shawn::ConstMessageHandle& mh)
    throw () {
        const SimpleConsensusMessage* simpleconsensusmsg =
        dynamic_cast<const SimpleConsensusMessage*> (mh.get());

        if (simpleconsensusmsg != NULL && owner_w() != simpleconsensusmsg->source()) {
            handle_flooding_message_node(*simpleconsensusmsg);
            return true;
        }

        return shawn::Processor::process_message(mh);
    }
    // ----------------------------------------------------------------------

    void
    SimpleConsensusProcessor::
    work(void)
    throw () {

		// An h pleiopshfousa timh einai h UNKNOWN_VALUE shmainei pos oi diergasies den exoun 
		// dextei ola ta mhnymata akoma opote h ka8e diergasia/kombos epeksergazetai thn lista
		// ths me tis times pou elabe apo tis alles diergasies kai stelnei ena neo mhnyma me 
		// thn kainourgia lista ths stis upoloipes diergasies
		if(maj_value == UNKNOWN_VALUE)
		{			
			// Bazoume tis times ths listas ths ka8e diergasias ston map freq_list etsi oste
			// na mporesoume na tis epeksergastoume kai na broume thn pleiopshfousa timh
			for(int i=1;i<node_count_;i++)
			{
				freq_list[value_list_[i]]++;
			}
			
			// Briskoume thn pleiopshfousa timh kai thn apo8hkeuoume sthn metablhth maj_value
			for(std::map<int, int>::iterator it = freq_list.begin(); it != freq_list.end(); ++it)
			{              
				if(it->second > max_freq)
				{
					max_freq = it->second;
					maj_value = it->first;
				}
			}
			
			//at each round send a message with the array of node values
			send(new SimpleConsensusMessage(value_list_,node_count_));
        }
        else {	//exoume pleiopshfousa timh diaforh ths UNKNOWN_VALUE
			
			// Apo8hkeuoume thn pleiopshfousa timh san thn timh eksodou pou apofasise h diergasia
			output_value_ = maj_value;
			
			INFO(logger() , id() << ": My output value(desicion) is :" << output_value_ << std::endl);		
			
			// Apenergopoioume thn diergasia/kombo
			set_state(Inactive);	
        }

    }
    // ----------------------------------------------------------------------

    void
    SimpleConsensusProcessor::
    handle_flooding_message_node(const SimpleConsensusMessage& simpleconsensusmsg)
    {
		// Handle the new simpleconsensus message
		// Merge the values we took with our list
		
		int *msg_values = simpleconsensusmsg.values_;
		
		// INFO(logger() ,owner_w().label() << " Received new array from : " << simpleconsensusmsg.source_w().label() << std::endl);
				
		for(int i=0; i < node_count_ ; i++)
		{
			if(msg_values[i] != UNKNOWN_VALUE)
			{
				value_list_[i] = msg_values[i];
			}
		}
    }
    // ----------------------------------------------------------------------

}
#endif
