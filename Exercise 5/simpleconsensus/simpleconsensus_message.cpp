/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_SIMPLECONSENSUS

#include "legacyapps/simpleconsensus/simpleconsensus_message.h"

namespace simpleconsensus
{

	// ----------------------------------------------------------------------
   SimpleConsensusMessage::
   SimpleConsensusMessage(int *values,int size)
    {
       setSize(size);
       values_ = new int[size];
       for(int i=0;i<size;i++)
           values_[i] = values[i];
    }
	// ----------------------------------------------------------------------
   SimpleConsensusMessage::
	~SimpleConsensusMessage()
	{
            delete[] values_;
        }

}
#endif
