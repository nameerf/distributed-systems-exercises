/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#ifndef __SHAWN_LEGACYAPPS_SIMPLECONSENSUS_MESSAGE_H
#define __SHAWN_LEGACYAPPS_SIMPLECONSENSUS_MESSAGE_H
#include "_legacyapps_enable_cmake.h"

#ifdef ENABLE_SIMPLECONSENSUS

#include "sys/message.h"
#include <string.h>

namespace simpleconsensus
{

   class SimpleConsensusMessage
       : public shawn::Message
   {
   public:
      SimpleConsensusMessage(int *,int);
      virtual ~SimpleConsensusMessage();

      int* values( void )
      {
          return values_;
      };
      
      int* values_;
   };

       
}

#endif
#endif
