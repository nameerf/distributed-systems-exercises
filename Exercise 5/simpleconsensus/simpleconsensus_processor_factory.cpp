/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_SIMPLECONSENSUS

#include "legacyapps/simpleconsensus/simpleconsensus_processor_factory.h"
#include "legacyapps/simpleconsensus/simpleconsensus_processor.h"
#include "sys/processors/processor_keeper.h"
#include "sys/simulation/simulation_controller.h"
#include <iostream>

using namespace std;
using namespace shawn;

namespace simpleconsensus
{

   // ----------------------------------------------------------------------
   void
   SimpleConsensusProcessorFactory::
   register_factory( SimulationController& sc )
   throw()
   {
      sc.processor_keeper_w().add( new SimpleConsensusProcessorFactory );
   }
   // ----------------------------------------------------------------------
   SimpleConsensusProcessorFactory::
   SimpleConsensusProcessorFactory()
   {
      //cout << "HelloworldProcessorFactory ctor" << &auto_reg_ << endl;
   }
   // ----------------------------------------------------------------------
   SimpleConsensusProcessorFactory::
   ~SimpleConsensusProcessorFactory()
   {
      //cout << "HelloworldProcessorFactory dtor" << endl;
   }
   // ----------------------------------------------------------------------
   std::string
   SimpleConsensusProcessorFactory::
   name( void )
   const throw()
   {
      return "simpleconsensus";
   }
   // ----------------------------------------------------------------------
   std::string
   SimpleConsensusProcessorFactory::
   description( void )
   const throw()
   {
      return "Shawn simpleconsensus Processor";
   }
   // ----------------------------------------------------------------------
   shawn::Processor*
   SimpleConsensusProcessorFactory::
   create( void )
   throw()
   {
      return new SimpleConsensusProcessor;
   }
}

#endif
