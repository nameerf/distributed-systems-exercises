/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#ifndef __SHAWN_LEGACYAPPS_SIMPLECONSENSUS_PROCESSOR_H
#define __SHAWN_LEGACYAPPS_SIMPLECONSENSUS_PROCESSOR_H
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_SIMPLECONSENSUS

#include "sys/processor.h"
#include "sys/event_scheduler.h"
#include "legacyapps/simpleconsensus/simpleconsensus_message.h"
#include <string.h>

namespace simpleconsensus
{

   /**
    */
   class SimpleConsensusProcessor
       : public shawn::Processor
   {
   public:
      ///@name Constructor/Destructor
      ///@{
      SimpleConsensusProcessor();
      virtual ~SimpleConsensusProcessor();
      ///@}

      ///@name Inherited from Processor
      ///@{
      /**
       */
      virtual void boot( void ) throw();
      /**
       */
      virtual bool process_message( const shawn::ConstMessageHandle& ) throw();
      /**
       */
      virtual void work( void ) throw();
      ///@}

      virtual void special_boot( void ) throw();

   private:
      ///@name Message Handling
      ///@{
      /**
       */
      void handle_flooding_message_node( const SimpleConsensusMessage& flooding );
      /**
       */
      
       //// The diameter of the network (pass at the beginning of the simulation from the .conf file)
      int diameter_;

       //// The initial value of this node that read its as input from a tag
      int input_value_;

       //// The ouput value of this node 
      int output_value_;

       ////  The tag that contains the input value for this node (is loaded from the .xml file)
      shawn::IntegerTag *input_tag_; 

       //// The list of values currently known to this node
      int *value_list_;

       //// The total number of nodes for this simulation
      int node_count_;

	   //// Mia agnosth timh me thn opoia arxikopoioume th lista tou ka8e kombou
      int UNKNOWN_VALUE;
      
       //// H megisth syxnothta emfanishs mias timhs (pou ka8orizei telika kai thn pleiopshfousa timh)
      int max_freq;
      
       //// H pleiopshfousa timh
      int maj_value;
      
       //// Enas map pou antistoixei thn syxnothta emfanishs mias timhs me thn timh
      std::map<int,int> freq_list;

   };

}

#endif
#endif
