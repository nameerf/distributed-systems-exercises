/************************************************************************
** This file is part of the network simulator Shawn.                  **
** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
** Shawn is free software; you can redistribute it and/or modify it   **
** under the terms of the BSD License. Refer to the shawn-licence.txt **
** file in the root of the Shawn source tree for further details.     **
************************************************************************/
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_FLOODMAX

#include "legacyapps/floodmax/floodmax_processor.h"
#include "legacyapps/floodmax/floodmax_message.h"
#include "sys/simulation/simulation_controller.h"
#include "sys/node.h"
#include <iostream>
#include <limits>
#include <cmath>

namespace floodmax
{
    FloodMaxProcessor::
    FloodMaxProcessor()
    {}
    // ----------------------------------------------------------------------

    FloodMaxProcessor::
    ~FloodMaxProcessor()
    {}
    // ----------------------------------------------------------------------

    void
    FloodMaxProcessor::
    special_boot( void )
        throw()
    {}
    // ----------------------------------------------------------------------

    void FloodMaxProcessor::boot(void) throw()
    {
        //pairnoume thn timh ths metablhths diameter pou exoume orisei ston configuration file floodmax.conf
        const shawn::SimulationEnvironment& se = owner().world().simulation_controller().environment();
        diameter_ = se.optional_int_param("diameter", 0);

        //initialize the algorithm variables
        leader_ = false; //oles oi diergasies 8etoun leader = false
        max_id_ = owner_w().id();	//arxika o ka8e kombos 8etei to diko tou id san to megisto
        //std::cout << "The id of node " << owner_w().label() << " is: " << owner_w().id();
        //std::cout << " and has max_id: " << max_id_ << std::endl;
    }
    // ----------------------------------------------------------------------

    bool
    FloodMaxProcessor::
    process_message( const shawn::ConstMessageHandle& mh )
        throw()
    {
        const FloodMaxMessage* floodmaxmsg = dynamic_cast<const FloodMaxMessage*> ( mh.get() );

        if ( floodmaxmsg != NULL && owner_w() != floodmaxmsg->source())
        {
            handle_flooding_message_node( *floodmaxmsg );
            return true;
        }

        return shawn::Processor::process_message( mh );
    }
    // ----------------------------------------------------------------------

    void FloodMaxProcessor::work(void) throw()
    {
        //stelnoume thn megisth, mexri tora, taytothta se olous tous geitones
        send(new FloodMaxMessage(max_id_));
    }
    // ----------------------------------------------------------------------

    void FloodMaxProcessor::handle_flooding_message_node(const FloodMaxMessage& floodmaxmsg) throw()
    {
        //Handle the new floodmax message

        //an h taytothta poy molis phrame einai megalyterh apo thn trexonta megisth 8ese thn nea taytotha os megisth
        if(floodmaxmsg.id() > max_id_) {
            max_id_ = floodmaxmsg.id();
            //std::cout << "The node " << owner_w().label() << " changed its max_id to : " << max_id_ << std::endl;
        }

        //std::cout << "The id of node " << owner_w().label() << " is: " << owner_w().id();
        //std::cout << " and has max_id: " << max_id_ << std::endl;

        //exoun perasei diameter gyroi?
        if(simulation_round() == diameter_) {
            //an to id ths diergasias einai iso me to maximum 8ese leader = true gia thn sygkekrimenh diergasia
            if(owner_w().id() == max_id_)
                leader_ = true;

            //exoun perasei diameter sto plh8os guroi opote apenergopoihse oles tis diergasies
            set_state(shawn::Processor::Inactive);
        }

        //exoume ekleksei arxhgo
        if(leader_) {
            std::cout << "We have a leader!" << std::endl;
            std::cout << "The leader is the node " << owner_w().label() << " with id = max_id = " << max_id_ << std::endl;
        }
    }
    // ----------------------------------------------------------------------

}
#endif
