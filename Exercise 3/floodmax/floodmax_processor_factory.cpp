/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_FLOODMAX

#include "legacyapps/floodmax/floodmax_processor_factory.h"
#include "legacyapps/floodmax/floodmax_processor.h"
#include "sys/processors/processor_keeper.h"
#include "sys/simulation/simulation_controller.h"
#include <iostream>

using namespace std;
using namespace shawn;

namespace floodmax
{

   // ----------------------------------------------------------------------
   void
   FloodMaxProcessorFactory::
   register_factory( SimulationController& sc )
   throw()
   {
      sc.processor_keeper_w().add( new FloodMaxProcessorFactory );
   }
   // ----------------------------------------------------------------------
   FloodMaxProcessorFactory::
   FloodMaxProcessorFactory()
   {
      //cout << "HelloworldProcessorFactory ctor" << &auto_reg_ << endl;
   }
   // ----------------------------------------------------------------------
   FloodMaxProcessorFactory::
   ~FloodMaxProcessorFactory()
   {
      //cout << "HelloworldProcessorFactory dtor" << endl;
   }
   // ----------------------------------------------------------------------
   std::string
   FloodMaxProcessorFactory::
   name( void )
   const throw()
   {
      return "floodmax";
   }
   // ----------------------------------------------------------------------
   std::string
   FloodMaxProcessorFactory::
   description( void )
   const throw()
   {
      return "Shawn floodmax Processor";
   }
   // ----------------------------------------------------------------------
   shawn::Processor*
   FloodMaxProcessorFactory::
   create( void )
   throw()
   {
      return new FloodMaxProcessor;
   }
}

#endif
