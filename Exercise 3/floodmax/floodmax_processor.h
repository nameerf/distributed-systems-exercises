/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#ifndef __SHAWN_LEGACYAPPS_FLOODMAX_PROCESSOR_H
#define __SHAWN_LEGACYAPPS_FLOODMAX_PROCESSOR_H
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_FLOODMAX

#include "sys/processor.h"
#include "sys/event_scheduler.h"
#include "legacyapps/floodmax/floodmax_message.h"
#include <string.h>

namespace floodmax
{

   /**
    */
   class FloodMaxProcessor
       : public shawn::Processor
   {
   public:
      ///@name Constructor/Destructor
      ///@{
      FloodMaxProcessor();
      virtual ~FloodMaxProcessor();
      ///@}

      ///@name Inherited from Processor
      ///@{
      /**
       */
      virtual void boot( void ) throw();
      /**
       */
      virtual bool process_message( const shawn::ConstMessageHandle& ) throw();
      /**
       */
      virtual void work( void ) throw();
      ///@}

      virtual void special_boot( void ) throw();

   private:
      ///@name Message Handling
      ///@{
      /**
       */
      void handle_flooding_message_node( const FloodMaxMessage& flooding ) throw();
      /**
       */
      
      /// just set to true when 'leader node' (initially false)
       bool leader_;

       //// The max id the node has seen so far (initially our id)
       int max_id_;

       //// The diameter of the network (pass at the beginning of the simulation from the .conf file)
       int diameter_;
       
   };

}

#endif
#endif
