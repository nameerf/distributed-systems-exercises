/************************************************************************
 ** This file is part of the network simulator Shawn.                  **
 ** Copyright (C) 2004-2007 by the SwarmNet (www.swarmnet.de) project  **
 ** Shawn is free software; you can redistribute it and/or modify it   **
 ** under the terms of the BSD License. Refer to the shawn-licence.txt **
 ** file in the root of the Shawn source tree for further details.     **
 ************************************************************************/
#include "_legacyapps_enable_cmake.h"
#ifdef ENABLE_FLOODMAX

#include <iostream>
#include "legacyapps/floodmax/floodmax_init.h"
#include "legacyapps/floodmax/floodmax_processor_factory.h"
#include "sys/simulation/simulation_controller.h"
#include "sys/simulation/simulation_task_keeper.h"
#include "sys/transm_models/transmission_model_keeper.h"


extern "C" void init_floodmax( shawn::SimulationController& sc )
{
   USER( "Initialising floodmax module (floodmax)" );
   floodmax::FloodMaxProcessorFactory::register_factory( sc );

}

#endif
